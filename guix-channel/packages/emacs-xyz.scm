(define-module (guix-channel packages emacs-xyz)
  #:use-module (gnu packages)
  #:use-module (gnu packages emacs-xyz)
  #:use-module (gnu packages fonts)	; for emacs-all-the-icons
  #:use-module (guix git-download)
  #:use-module (guix packages)
  #:use-module (guix build-system emacs)
  #:use-module ((guix licenses) #:prefix license:))

(define-public my-emacs-citar-denote
  (let ((commit "f093c37d28320a04b5e7ee87d5c442fefa749c35")
	(revision "0"))
    (package
      (name "emacs-citar-denote")
      (version "1.8")
      (source
       (origin
	 (method git-fetch)
	 (uri (git-reference
               (url "https://github.com/pprevos/citar-denote")
               (commit commit)))
	 (file-name (git-file-name name version))
	 (sha256
          (base32
           "1bnyqvdawyxrafz7c79f6xq5h90rhdakmv6g1jhplh881zmkq25w"))))
      (build-system emacs-build-system)
      (propagated-inputs
       (list emacs-citar
	     emacs-dash
	     emacs-denote))
            (license license:gpl3)
      (home-page "https://github.com/pprevos/citar-denote")
      (synopsis "Integrating the Emacs Citar and Denote packages")
      (description
       "This package integrating the Emacs Citar and Denote packages to enable
managing bibliographic notes and citations."))))

(define-public my-emacs-citar-fix
  (let ((commit "572b7b6e569e9423dd948539fa48d3f53ceffe57")
	(last-release-version "1.4.0")
	(revision "0"))
    (package
      (inherit emacs-citar)
      (name "emacs-citar-fix")
      (version (git-version last-release-version revision commit))
      (source (origin
		(method git-fetch)
		(uri (git-reference
                      (url "https://github.com/bdarcus/citar")
                      (commit commit)))
		(file-name (git-file-name name version))
		(sha256
		 (base32
                  "17qfkiwy2mvyp6rwkxwxhlx2lxw5j2rb7n15c3dyzvfdphxjfikd")))))))

(define-public my-emacs-golden-ratio
  (let ((commit "375c9f287dfad68829582c1e0a67d0c18119dab9")
	(revision "0"))
    (package
      (name "emacs-golden-ratio")
      (version "1.0.1")
      (source
       (origin
	 (method git-fetch)
	 (uri (git-reference
               (url "https://github.com/roman/golden-ratio.el")
               (commit commit)))
	 (file-name (git-file-name name version))
	 (sha256
          (base32
           "0a635a3h6jx0clgwmhwc48i14y3xy5q29y37lp2sjnbxx1hlmkli"))))
      (build-system emacs-build-system)
            (license license:gpl3)
      (home-page "https://github.com/roman/golden-ratio.el")
      (synopsis "Automatic window resizing")
      (description
       "This package allows to resize the focused window automatically according
to the golden ratio."))))

(define-public my-emacs-native-shell-complete
  (let ((commit "7b5e7d86c39ce9833118db278034789a6c0ecfd6")
	(revision "0"))
    (package
      (name "emacs-native-shell-complete")
      (version "0.1.0")
      (source
       (origin
	 (method git-fetch)
	 (uri (git-reference
               (url "https://github.com/CeleritasCelery/emacs-native-shell-complete")
               (commit commit)))
	 (file-name (git-file-name name version))
	 (sha256
          (base32
           "0khlr0g6b5ml3n0s49mzhlwfca7gcxf07sb2h6014vnhf1i7qy78"))))
      (build-system emacs-build-system)
      (propagated-inputs
       (list emacs-company))
      (license license:gpl3)
      (home-page "https://github.com/CeleritasCelery/emacs-native-shell-complete")
      (synopsis "Native completion for emacs shells")
      (description
       "This package provides the exact same completions
        in shell-mode as you get in a terminal emulator
        with the TAB key. This means that completions are
        always accurate and synchronized with your current
        directory and shell environment. This also means that subshell
        completion works correctly."))))

(define-public my-emacs-nerd-icons-completion
  (let ((commit "c2db8557a3c1a9588d111f8c8e91cae96ee85010")
	(revision 0))
    (package
      (name "emacs-nerd-icons-completion")
      (version "0.0.1")
      (source
       (origin
	 (method git-fetch)
	 (uri (git-reference
               (url "https://github.com/rainstormstudio/nerd-icons-completion")
	       (commit commit)))
	 (file-name (git-file-name name version))
	 (sha256
          (base32
           "10ll0dj6ym5prrkv6smj0ac2ail4b3rqcrh1lyr61y3cj422vn9z"))))
      (build-system emacs-build-system)
      (propagated-inputs
       (list emacs-nerd-icons))
      (license license:gpl3)
      (home-page "https://github.com/rainstormstudio/nerd-icons-completion")
      (synopsis "Use nerd-icons for completion.")
      (description
       "nerd-icons-completion is inspired by all-the-icons-completion."))))

(define-public my-emacs-nerd-icons-dired
  (let ((commit "c1c73488630cc1d19ce1677359f614122ae4c1b9")
	(revision 0))
    (package
      (name "emacs-nerd-icons-dired")
      (version "0.0.1")
      (source
       (origin
	 (method git-fetch)
	 (uri (git-reference
               (url "https://github.com/rainstormstudio/nerd-icons-dired")
	       (commit commit)))
	 (file-name (git-file-name name version))
	 (sha256
          (base32
           "1ln73ii7c3chl4lvarwiwrdmx49q528wc0h6a7xbl68pc2pyyvq2"))))
      (build-system emacs-build-system)
      (propagated-inputs
       (list emacs-nerd-icons))
      (license license:gpl3)
      (home-page "https://github.com/rainstormstudio/nerd-icons-dired")
      (synopsis "Use nerd-icons for Dired.")
      (description
       "nerd-icons-dired is inspired by all-the-icons-dired"))))

(define-public my-emacs-shell-command-x
  (let ((commit "5ad0a0270e22e6f89f2163e2dc65a0f39915793b")
	(revision "0"))
    (package
      (name "emacs-shell-command-x")
      (version "0.1.3")
      (source
       (origin
	 (method git-fetch)
	 (uri (git-reference
               (url "https://github.com/elizagamedev/shell-command-x.el")
               (commit commit)))
	 (file-name (git-file-name name version))
	 (sha256
          (base32
           "1irgvim6b3ncdf1612r4z9rr7d6fymg7fs072pgkgcd7c6cs1h49"))))
      (build-system emacs-build-system)
      (propagated-inputs
       (list emacs-company))
      (license license:gpl3)
      (home-page "https://github.com/elizagamedev/shell-command-x.el")
      (synopsis "Extensions for Emacs' shell commands.")
      (description
       "This package provides provides an assortment of extensions for
the interactive Emacs shell commands"))))

(define-public my-emacs-spacious-padding
  (let ((commit "0d9f3bcf44e56b473124fc89289b36c01cc17535")
	(revision "0"))
    (package
      (name "emacs-spacious-padding")
      (version "0.1.0")
      (source
       (origin
	 (method git-fetch)
	 (uri (git-reference
               (url "https://git.sr.ht/~protesilaos/spacious-padding")
               (commit commit)))
	 (file-name (git-file-name name version))
	 (sha256
          (base32
           "1nqas3divxxprgi815pkb81a511kpnfmhvl3n3x8xd81ksfn73s6"))))
      (build-system emacs-build-system)
      (license license:gpl3)
      (home-page "https://git.sr.ht/~protesilaos/spacious-padding")
      (synopsis "Increase the padding/spacing of GNU Emacs frames and windows ")
      (description
       "This package provides a global minor mode to increase the
spacing/padding of Emacs windows and frames. "))))

;; There is a bug in the version packaged by rde
(define-public my-emacs-zotra
  (let ((commit "fe9093b226a1678fc6c2fadd31a09d5a22ecdcf1")
        (revision "0"))
    (package
      (name "emacs-zotra")
      (version (git-version "1.0" revision commit))
      (source
       (origin
         (method git-fetch)
         (uri (git-reference
               (url "https://github.com/mpedramfar/zotra")
               (commit commit)))
         (file-name (git-file-name name version))
         (sha256
          (base32
           "04a7h183kbl8nfkhn2386yljmv7hgxg0cdyw1ir3x60i3nji179z"))))
      (build-system emacs-build-system)
      (license license:gpl3)
      (home-page "https://github.com/mpedramfar/zotra")
      (synopsis "Get bibliographic information from a url")
      (description
       "This emacs library provides functions to get bibliographic information
 from a url and save it into a bibtex file. It also provides a way to obtain a
 list of attachments (e.g. PDF files) associated with a url. This is done
 using Zotero translators, but without using the Zotero client."))))

;;emacs-native-shell-complete
