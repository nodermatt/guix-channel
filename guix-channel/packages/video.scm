(define-module (guix-channel packages video)
  #:use-module (gnu packages)
  #:use-module (gnu packages backup)
  #:use-module (gnu packages video)
  #:use-module (guix git-download)
  #:use-module (guix packages)
  #:use-module (guix build-system node)
  #:use-module (guix gexp)
  #:use-module (guix utils)
  #:use-module ((guix licenses) #:prefix license:))

(define-public mpv-with-libarchive
  (package/inherit mpv
    (name "mpv-with-libarchive")
    (arguments
     (substitute-keyword-arguments (package-arguments mpv)
       ((#:configure-flags flags #~'())
        #~(cons* "--enable-libarchive" #$flags))))
    (inputs (modify-inputs (package-inputs mpv)
	      (prepend libarchive)))))
