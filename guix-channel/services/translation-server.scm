(define-module (guix-channel services translation-server)
  #:use-module (gnu services)
  #:use-module (gnu services configuration)
  #:use-module (gnu services docker)
  #:use-module (gnu system shadow)
  #:use-module (guix diagnostics)
  #:use-module (guix gexp)
  #:use-module (guix i18n)
  #:use-module (ice-9 match)
  #:export (translation-server-configuration
	    translation-server-configuration?
	    translation-server-configuration-fields
	    translation-server-configuration-image
	    translation-server-configuration-port

	    translation-server-configuration->oci-container-environment
	    translation-server-configuration->oci-container-configuration

	    oci-translation-server-service-type))

(define translation-server-tag
  "2.0.4")

(define translation-server-image
  (string-append "zotero/translation-server:" translation-server-tag))

;; Turn field names, which are Scheme symbols into strings
(define (uglify-field-name field-name)
  (symbol->string field-name))

(define (serialize-string field-name value)
  (cons (uglify-field-name field-name) value))

(define (serialize-maybe-string field-name value)
  (if (maybe-value-set? value)
      (serialize-string field-name value)
      '()))

(define (serialize-boolean field-name value)
  (serialize-string field-name (if value "true" "false")))

(define (serialize-list-of-strings field-name value)
  (define (format-value val)
    (string-append "["
                   (string-join
                    (map (lambda (s) (string-append "\"" s "\""))
                         val)
                    ", ")
                   "]"))
  (serialize-string field-name (format-value value)))

(define-configuration/no-serialization translation-server-configuration
  (image
   (string translation-server-image)
   "The image to use for the OCI backed Shepherd service.")
  (port
   (string "1969")
   "The port where translation-server will be exposed."))

;; TODO: DELETE ME
(define test-config
  (translation-server-configuration
   (port "1969")))

(define translation-server-configuration->oci-container-environment
  (lambda (config)
    (filter (compose not null?)
            (map (lambda (f)
                   (let ((field-name (configuration-field-name f))
                         (type (configuration-field-type f))
                         (value ((configuration-field-getter f) config)))
                     (if (not (eq? field-name 'image))
                         (match type
                           ('string
                            (serialize-string field-name value))
                           ('maybe-string
                            (serialize-maybe-string field-name value))
                           ('list-of-strings
                            (serialize-list-of-strings field-name value))
                           ('boolean
                            (serialize-boolean field-name value))
                           (_
                            (raise
                             (formatted-message
                              (G_ "Unknown translation-server-configuration field type: ~a")
                              type))))
                         '())))
                 translation-server-configuration-fields))))

;; oci-container-configuration needs the following fields:
;; user, group, command, entrypoint, environment
(define translation-server-configuration->oci-container-configuration
  (lambda (config)
    (let ((environment
           (translation-server-configuration->oci-container-environment config))
          (image
           (translation-server-configuration-image config))
          (port
           (translation-server-configuration-port config)))
      (list (oci-container-configuration
             (image image)
             (environment environment)
             (ports
              `((,port . ,port))))))))

(define oci-translation-server-service-type
  (service-type (name 'translation-server)
                (extensions (list (service-extension oci-container-service-type
                                                     translation-server-configuration->oci-container-configuration)
                                  ;; (service-extension account-service-type
                                  ;;                    (const %conduit-accounts))
                                  ;; (service-extension activation-service-type
                                  ;;                    %conduit-activation)
				  ))
                (default-value (translation-server-configuration))
                (description
                 "This service install a OCI backed Zotero Translation Server Shepherd Service.")))

;; TODO: DELETEME
;; (service oci-translation-server-service-type
;; 	 (translation-server-configuration))
